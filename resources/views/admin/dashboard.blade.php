@extends('layouts.admin_layout')
@section('css')
<link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
    </h1>
    <ol class="breadcrumb">
      <li><a href="/admin/index"><i class="fa fa-home"></i> Dashboard</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Total Pengajuan Portofolio</h3>
          </div>
          <div class="box-body">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="fa fa-trophy"></i></span>

                    <div class="info-box-content">
                      <span class='info-box-text'>Prestasi</span>
                      <span class="info-box-number">{{$prestasi}}</span>
                      <span class="progress-description" style="margin-top: 12px;">
                        <a href="{{route('admin.prestasi.index')}}" style="text-decorations:none; color:inherit;"><b>Selengkapnya</b></a>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-red">
                    <span class="info-box-icon"><i class="fa fa-paint-brush"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Sertifikat & Keahlian</span>
                      <span class="info-box-number">{{$sertifikasi}}</span>
                      <span class="progress-description" style="margin-top: 12px;">
                        <a href="{{route('admin.sertifikat.index')}}" style="text-decorations:none; color:inherit;"><b>Selengkapnya</b></a>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>

                <!-- /.col -->
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="fa fa-rocket"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Karya & Penelitian</span>
                      <b>Karya Personal {{$karya}}</b><br>
                      <b>Penelitian Ilmiah {{$penelitian}}</b>
                      <span class="progress-description" style="margin-top: 5px;">
                        <a href="{{route('admin.penelitian.index')}}" style="text-decorations:none; color:inherit;"><b>Selengkapnya</b></a>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box bg-aqua">
                    <span class="info-box-icon"><i class="fa fa-sitemap"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Pengalaman</span>
                      <b>Organisasi {{$pekerjaan}}</b><br>
                      <b>Pekerjaan {{$pekerjaan}}</b>
                      <span class="progress-description" style="margin-top: 5px;">
                        <a href="{{route('admin.pekerjaan.index')}}" style="text-decorations:none; color:inherit;"><b>Selengkapnya</b></a>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>

                <!-- /.col -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- detail News Modal -->
  

  @endsection
  @section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
  @stop